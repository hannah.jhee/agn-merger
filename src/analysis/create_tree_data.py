import sys
sys.path.append("../../notebooks")
from import_all import *
from multiprocessing import Pool

import pandas as pd
with open(f"../../snap_to_at.pickle","rb") as handle:
    snap_at = pickle.load(handle)

tqdm.pandas()

class Tree:
    def __init__(self, simul, hid):
        self.simul = simul
        self.hid = hid
        self.tree = None
        self.mmps = None
        self.merger = None

    def load_tree(self, file_format="/home/gpuadmin/HaloFinders/run_rockstar/results/%s/Rockstar/trees/tree_0_0_0.dat"):
        self.tree = tree_data(file_format%self.hid)
        self.tree['redshift'] = 1/np.array(itemgetter(*self.tree.Snap_idx.values)(snap_at))-1
        self.tree = self.tree[self.tree.Snap_idx>=4]
        self.tree = self.tree.astype({"Snap_idx":int})

        print(f"{self.hid} :: Getting stellar masses within 0.1rvir")
        self.tree['SM'] = self.tree[['Snap_idx','x','y','z','Rvir']].apply(lambda x: self._get_SM(self.hid, x.Snap_idx.astype(int), x.x, x.y, x.z, x.Rvir), axis=1)
        print(f"{self.hid} :: Getting gas masses within 0.1rvir")
        self.tree['GM'] = self.tree[['Snap_idx','x','y','z','Rvir']].apply(lambda x: self._get_GM(self.hid, x.Snap_idx.astype(int), x.x, x.y, x.z, x.Rvir), axis=1)

        print(f"{self.hid} :: Getting most massive progenitors")
        self._get_mmps()
        #self.tree = self.tree[(self.tree.Snap_idx>=19) & (self.tree.Snap_idx<=60)]
        print(f"{self.hid} :: Done")
        
    def _get_mmps(self):
        snapNum = 94
        mmp_id = -1

        self.mmps = pd.DataFrame(columns=self.tree.columns)
        self.merger = pd.DataFrame(columns=self.tree.columns)
        while True:
            tr = self.tree[self.tree.Snap_idx == snapNum]
            tr = tr[tr.desc_id==mmp_id]
            mmp = tr[tr.Mvir.values==tr.Mvir.values.max()]
            mmp_id = mmp.id.iat[0]
            num_progs = mmp.num_prog.iat[0]

            self.mmps = pd.concat([self.mmps,mmp])
            if num_progs==0: break # if it has no progenitor, stop the tracing

            if num_progs>1: # if the number of progenitor is >=2, then it suffered "merger"
                self.merger = pd.concat([self.merger, tr])
            
            if snapNum==4: break
        
            snapNum -= 1

#        self.mmps = self.mmps.sort_values(by="Snap_idx", ascending=True)


        self.mmps = self.mmps.sort_values(by="Snap_idx", ascending=True)
        self.merger = self.merger.sort_values(by="Snap_idx", ascending=True)
        self.mmps = self.mmps#[self.mmps.Snap_idx<61]
        self.merger = self.merger#[self.merger.Snap_idx<61]

    def _get_SM(self, halo, snapNum, hx, hy, hz, hrvir):
        with h5py.File(f"/data2/Fiducial_MrAGN/{halo}/snap_{halo}_sf_x_2x_{snapNum:03d}.hdf5") as f:
            scoords = f["PartType4"]["Coordinates"][:,:] * 1e-3
            smasses = f["PartType4"]["Masses"][:]
        return smasses[np.linalg.norm(scoords-np.array([hx,hy,hz]), axis=1) < hrvir*1e-3*0.1].sum() * 1e10
    
    def _get_GM(self, halo, snapNum, hx, hy, hz, hrvir):
        with h5py.File(f"/data2/Fiducial_MrAGN/{halo}/snap_{halo}_sf_x_2x_{snapNum:03d}.hdf5") as f:
            gcoords = f["PartType0"]["Coordinates"][:,:] * 1e-3
            gmasses = f["PartType0"]["Masses"][:]
        return gmasses[np.linalg.norm(gcoords-np.array([hx,hy,hz]), axis=1) < hrvir*1e-3*0.1].sum() * 1e10



#halos = dict()
#for hid in sorted([f for f in os.listdir("/data2/Fiducial_MrAGN/") if f.startswith("m")]):
#    print(hid)
#    halos[hid] = Tree("Fiducial_MrAGN", hid)
#    halos[hid].load_tree()#

#    with open(f"../../data_products/tree_{hid}.pickle", "wb") as handle:
#        pickle.dump(handle, halos, pickle.HIGHEST_PROTOCOL)


def main(hid):
    tree = Tree("Fiducial_MrAGN", hid)
    tree.load_tree()

    with open(f"../../data_products/tree_{hid}.pickle","wb") as handle:
        pickle.dump(tree, handle, pickle.HIGHEST_PROTOCOL)


if __name__ == '__main__':
#halos = sorted([f for f in os.listdir("/data2/Fiducial_MrAGN/") if f.startswith("m")])
    halos = ['m0094']#, ' m0094', 'm0125']
    with Pool(len(halos)) as p:
        p.map(main, halos)
