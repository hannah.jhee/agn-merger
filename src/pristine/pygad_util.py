import numpy as np
from astropy import units as u

def read_info(filename, fields):
    lines = open(filename).readlines()
    lines = [line.split(':') for line in lines]

    info_fields = [line[0].strip() for line in lines]
    
    info = dict()
    for field in fields:
        try:
            index = info_fields.index(field)
        except:
            raise ValueError(f"{field} does not exist in this info file. Available fields are: {info_fields[2:-1]}")

        x = lines[index][1].split('[')
        for i in range(len(x)):
            x[i] = x[i].strip()
            x[i] = x[i].strip(']')
        
        if len(x)!=1:
            if len(x[0])==0:
                del x[0]
            if len(x[-1])==0:
                del x[-1]

            val,unit = x
            val = val.split()
            for i in range(len(val)):
                val[i] = float(val[i])

        else: # if len(x)==1
            val = float(x[0])
            unit = None
        info[field] = [val, unit]

    return info
        