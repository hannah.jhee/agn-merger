import numpy as np
import pandas as pd

def tree_data(tree_path):
    data = np.loadtxt(tree_path, skiprows=49)
    columns = open(tree_path).readline().split()
    columns[0] = columns[0][1:]
    columns[0] = columns[0][1:]
    for i in range(len(columns)):
        col = columns[i]
        if col.endswith(')'):
            columns[i] = col.split('(')[0]        
    df = pd.DataFrame(columns=columns, data=data)
    offset = 94 - df['Snap_idx'].max()
    df['Snap_idx'] += offset
    
    return df.sort_values(by="Snap_idx", ascending=True).reset_index().drop("index",axis=1)


def trace_progenitors(treepath):
    tree = tree_data(treepath)
    pos = []
    mass = []

    snapNum = 94
    mmp_id = -1
    while True:
        tr = tree[tree.Snap_idx == snapNum]
        tr = tr[tr.desc_id==mmp_id]
        mmp = tr[tr.Mvir.values==tr.Mvir.values.max()]
        mmp_id = mmp.id.iat[0]
        num_progs = mmp.num_prog.iat[0]

        if snapNum<=60:
            pos.append(mmp[['x','y','z']].values.tolist()[0])
        if snapNum==19:
            break

        snapNum -= 1
    return pos[::-1]