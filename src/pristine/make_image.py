#!/usr/bin/env python
from __future__ import print_function

import argparse
try:
    import ConfigParser
except ModuleNotFoundError:
    import configparser as ConfigParser
    
import glob

import numpy as np
from hyperion.model import ModelOutput
import astropy.units as u
from astropy.cosmology import Planck13
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

""" Convenience script to make images observed with an input filter, for use with Powderday and GADGET-3 simulations. Convolves
the simulation image with the input transmission curve, and saves it to a standard image size.
"""




def load_filter(filter_file):
    filt, trans = np.loadtxt(filter_file, dtype="double").T
    return filt, trans


def load_redshift(hydro_dir, halo, snap, backup_hydro_dir='./'):
    try:
        info_file = f"{hydro_dir}/{halo}/info_{int(snap):03d}.txt"
        open(info_file)
    except IOError:
        info_file = backup_hydro_dir + halo + "/info_" + snap + ".txt"

    with open(info_file) as f:
        lines = f.readlines()
        return float(lines[4].split()[1])  # Redshift is line 5 in info_*.txt

def filename(image_loc, fits_output=False, SED_output=False):
    root_filename_idx = image_loc.find(".rtout")
    root_filename = image_loc[:root_filename_idx]

    if (fits_output == True) & (SED_output == False):
        pattern = "{}.image.fits"
    elif (fits_output == True) & (SED_output == True):
        pattern = "{}.SED.fits"
    elif (fits_output == False) & (SED_output == True):
        pattern = "{}.SED.png"
    else:
        pattern = "{}.image.png"

    return pattern.format(root_filename)


def convolve_filter(image, filt, trans, units="ergs/cm^2/s"):
    imvals = image.val[
        0, :, :, 0]  # Pull out first orientation, image at first wavelength
    print("Convolving image with filter")

    for idx, fil in enumerate(filt):  # For each filter value in filter file
        wav = fil  # Wavelength is equal to value at that line
        iwav = np.argmin(np.abs(wav -
                                image.wav))  # Find nearest wavelength in image
        throughput = trans[idx]  # Identify corresponding transmission value
        imvals += (
            image.val[0, :, :, iwav]
        ) * throughput  # Add up all the image wavelength vals, convolve with transmission curve
    #eps = totShowVal.max() / 1e7
    return imvals


def make_image(image_file, distance, filt, trans):
    m = ModelOutput(image_file)
    hyperion_image = m.get_image(distance=distance.value,
                                 units="ergs/cm^2/s")  # Extract the image
    image = convolve_filter(hyperion_image, filt, trans)

    return image, hyperion_image


def save_image(image, hyperion_image, image_file, filter_file, distance,
               redshift, fits_output=False):

    output_image = filename(image_file, fits_output, SED_output=False)

    image_width = 2 * (hyperion_image.x_max * u.cm)

    if fits_output:
        from astropy.io import fits

        hdr = fits.Header()
        hdr['FILTER'] = filter_file.strip('.filter')
        hdr['REDSHIFT'] = redshift
        hdr['DISTANCE'] = (distance.to(u.kpc).value, 'kpc')
        hdr['WIDTH'] = (image_width.to(u.kpc).value, 'kpc')
        hdr['PIX_KPC'] = ((image_width / len(image)).to(u.kpc).value, 'kpc')
        hdr['BUNIT'] = 'erg/cm^2/s^2'

        hdu = fits.PrimaryHDU(image, header=hdr)
        hdul = fits.HDUList([hdu])
        hdu.writeto(output_image, overwrite=True)

    else:
        f, ax = plt.subplots(figsize=(6, 6), nrows=1, ncols=1)

        ax.set_facecolor("black")
        ax.imshow(image, origin="lower", cmap=plt.cm.Greys_r)

        w = (image_width * u.cm).to(u.kpc).value
        npix = image.shape[0]
        ax.set_xticks(np.linspace(0, npix, 5))
        ax.set_yticks(np.linspace(0, npix, 5))

        ticklabels = np.linspace(-w / 2, w / 2, 5)
        ticklabels = ['{:g}'.format(tick) for tick in ticklabels]
        ax.set_xticklabels(ticklabels)
        ax.set_yticklabels(ticklabels)

        ax.set_ylabel("y [kpc]", fontsize=26)
        ax.set_xlabel("x [kpc]", fontsize=26)
        f.tight_layout()
        f.savefig(output_image, bbox_inches="tight")
        plt.close(f)

    print("Saving image to :", output_image)


def make_sed(sed_file, distance):
    m = ModelOutput(sed_file)
    sed = m.get_sed(distance=distance.value, inclination="all", aperture=-1,
                    units="ergs/cm^2/s")
    return sed


def save_sed(sed, sed_file, filter_file, distance, redshift,
             fits_output=False):
    output_sed = filename(sed_file, fits_output, SED_output=True)

    wav, flux = sed

    if fits_output:
        from astropy.io import fits

        hdr = fits.Header()
        hdr['FILTER'] = filter_file.strip('.filter')
        hdr['REDSHIFT'] = redshift
        hdr['DISTANCE'] = (distance.to(u.kpc).value, 'kpc')
        hdr['BUNIT'] = 'erg/cm^2/s'

        hdu = fits.PrimaryHDU([wav, flux[0, :]], header=hdr)
        hdu.writeto(output_sed, overwrite=True)

    else:
        f, ax = plt.subplots(figsize=(6, 6), nrows=1, ncols=1)

        ax.loglog(wav, flux[0, :])

        ax.set_xlim(0.1, 1000)
        ax.set_ylim(1e-20, 1e-8)
        ax.set_xlabel(r"$\lambda$ [$\mu$m]", fontsize=26)
        ax.set_ylabel(r"$\lambda F_\lambda$ [erg/s/cm$^2$]", fontsize=26)
        f.tight_layout()
        f.savefig(output_sed, bbox_inches="tight")

    print("Saving SED to :", output_sed)

"""
if __name__=="__main__":
    p = argparse.ArgumentParser()
    p.add_argument("--home", dest="home")
    p.add_argument("--halo", dest="halo", default="m0948")
    p.add_argument("--filter", dest="filter", default="WFC3_F160W")
    p.add_argument("--snapNum", dest="snapNum", type=int, default=21)
    p.add_argument("--bins", dest="bins", type=int, default=256)
    args = p.parse_args()


    filter_dir = f"{args.home}/agn/inputs/filters/" #project_dir + "filters/"
    image_dir_pattern = f"{args.home}/agn/outputs/{args.halo}/{args.filter}/{args.bins}/"

    IMAGE = True
    SED = True
    FITS_IMAGE = True
    FITS_SED = True
#

    redshift = load_redshift(f"{args.home}/agn/inputs/data/", args.halo, args.snapNum)
    filter_file = f"{args.filter}.filter"
    filt,trans = load_filter(f"{args.home}/agn/inputs/filters/{filter_file}")
    distance = Planck13.luminosity_distance(redshift).cgs

    if IMAGE:
        image_file = f"{args.home}/agn/outputs/{args.halo}/{args.filter}/{args.bins}/output.{args.snapNum:03d}.rtout.image"
        try:
            image, hyperion_image = make_image(image_file, distance, filt, trans)
            save_image(image, hyperion_image, image_file, filter_file, distance, redshift, fits_output=FITS_IMAGE)
        except IOError:
            print("Failed: ", str(image_file))

    if SED:
        sed_file = f"{args.home}/agn/outputs/{args.halo}/{args.filter}/{args.bins}/output.{args.snapNum:03d}.rtout.sed"
        try:
            sed = make_sed(sed_file, distance)
            save_sed(sed, sed_file, filter_file, distance, redshift, fits_output=FITS_SED)
        except IOError:
            print("Failed: ", str(sed_file))
        """

"""
image_dirs = glob.glob(image_dir_pattern)
for image_dir in image_dirs:
    config_file = f"{args.home}/agn/inputs/Config.ini"

    parser = ConfigParser.ConfigParser()
    parser.read_file(open(config_file))

    # Read in directories from config_file
    hydro_dir = str(parser.get("pd-dir", "hydro_dir"))
    output_dir = str(parser.get("pd-dir", "output_dir"))
    backup_hydro_dir = str(parser.get("pd-dir", "backup_hydro_dir"))
    #

    # Read in parameters from config_file
    halo = str(parser.get("pd-run", "halo"))
    snap = str(parser.get("pd-run", "snap_num"))
    filter_file = str(parser.get("pd-run", "filter_file"))
    #

    # Read in redshift, filter wavelengths, and filter transmission curve,
    redshift = load_redshift(hydro_dir, halo, snap, backup_hydro_dir)
    filt, trans = load_filter(filter_file)
    distance = Planck13.luminosity_distance(redshift).cgs
    #

    # Form image of halo, convolve with input filter, and save plot
    if IMAGE:
        image_files = glob.glob(image_dir + "*.rtout.image")
        for image_file in image_files:
            try:
                image, hyperion_image = make_image(image_file, distance, filt,
                                                    trans)
                save_image(image, hyperion_image, image_file, filter_file,
                            distance, redshift, fits_output=FITS_IMAGE)
            except IOError:
                print("Failed: ", str(image_file))
                continue

    if SED:
        sed_files = glob.glob(image_dir + "*.rtout.sed")
        for sed_file in sed_files:
            try:
                sed = make_sed(sed_file, distance)
                save_sed(sed, sed_file, filter_file, distance, redshift,
                         fits_output=FITS_SED)
            except IOError:
                print("Failed: ", str(sed_file))
                continue
"""
