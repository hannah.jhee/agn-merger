#!/home/hannah.jhee/miniconda3/envs/merger/bin/python
import os
import pickle
import astropy
import numpy as np
import argparse
import configparser
import subprocess
from astropy.cosmology import Planck13
from astropy.io import fits
from astropy import units as u

import scipy
import photutils
from photutils.utils import calc_total_error
from photutils.segmentation import SegmentationImage
from astropy.stats import gaussian_fwhm_to_sigma
from astropy.convolution import Gaussian2DKernel
import statmorph

HOME = os.path.expanduser("~")
if HOME.split("/")[:-1] == 'gpuadmin':
    HOME = HOME + "/agn"

import sys
sys.path.append(f"{HOME}/agn/src/pristine/")
import pygad_util as pu
import tree_util as tu
import make_image as mi

sys.path.append(f"{HOME}/agn/src/realistic/")
import pristine_to_mock as ptm
import RealSim as rs


morph_params = {
    "GINI": "gini",
    "M20": "m20",
    "CONC": "concentration",
    "ASYM": "asymmetry",
    "SMOOTH": "smoothness",
    "SERSIC_N": "sersic_n",
    "SERSIC_A": "sersic_amplitude",
    "SERSIC_REFF": "sersic_rhalf",
    "SERSIC_XC": "sersic_xc",
    "SERSIC_YC": "sersic_yc",
    "SERSIC_ELLIP": "sersic_ellip",
    "SERSIC_THETA": "sersic_theta",
    "M": "multimode",
    "D": "deviation",
    "I": "intensity",
    "FLAG": "flag",
    "FLAG_SERSIC": "flag_sersic",
}

filt_wheel = configparser.ConfigParser()
filt_wheel.read_file(open(f"{HOME}/agn/inputs/filters/filter_wheel.txt"))

with open(f"{HOME}/agn/src/snap_to_at.pickle","rb") as handle:
    snap_at = pickle.load(handle)

def converter(angular_size, redshift):
    angular_size = angular_size * u.arcsec.to(u.rad)
    linear_size_mpc = Planck13.angular_diameter_distance(redshift) * angular_size
    linear_size_kpc = linear_size_mpc.to(u.kpc)
    return linear_size_kpc.value

def write_master_file(args):
    script =\
f"""
from __future__ import print_function
import os

pd_source_dir = '{HOME}/powderday/'

# ===============================================
# RESOLUTION KEYWORDS
# ===============================================
oref = 0
n_ref = 16
zoom_box_len = {args.boxsize}/2
bbox_lim = 1e5

# ===============================================
# PARALLELIZATION
# ===============================================
n_processes = 8
n_MPI_processes = 8

# ===============================================
# Radiative Transfer Information
# ===============================================
n_photons_initial = 1.e7
n_photons_imaging = 1.e7
n_photons_raytracing_sources = 1.e7
n_photons_raytracing_dust = 1.e7

FORCE_RANDO_SEED = False
seed = -12345

#===============================================
#DUST INFORMATION 
#===============================================
dustdir = "{HOME}/hyperion-dust-0.1.0/dust_files/"
dustfile = "d03_3.1_6.0_A.hdf5"
PAH = False  # WHAT IS THIS?

dust_grid_type = "dtm"
dusttometals_ratio = 0.4
enforce_energy_range = False

SUBLIMATION = False
SUBLIMATION_TEMPERATURE = 1600.0

#===============================================
#STELLAR SEDS INFO
#===============================================
FORCE_BINNED = True
COSMOFLAG = True
max_age_direct  = 1.e-2
imf_type = 2
pagb = 1

#===============================================
#NEBULAR EMISSION INFO
#===============================================
add_neb_emission = False
add_agb_dust_model = True
FORCE_gas_logu = False
gas_logu = -2
FORCE_gas_logz = False
gas_logz = 0

HII_T = 1.e4
HII_nh = 1.e2
HII_max_age = 2.e-3
neb_file_output = True
stellar_cluster_mass = 1.e4

CF_on = False
birth_cloud_clearing_age = 0.01

Z_init = 0.02

disk_stars_age = 8
bulge_stars_age = 8
disk_stars_metals = 19
bulge_stars_metals = 19

N_STELLAR_AGE_BINS = 30
metallicity_legend = "{HOME}/{args.metallicity}"

# ===============================================
# BLACK HOLE STUFF
# ===============================================
BH_SED = True
BH_eta = 0.1
BH_model = "{args.BH_model}"
BH_modelfile = "{HOME}/agn/inputs/BH_model/clumpy_models_201410_tvavg.hdf5"
BH_var = True
nenkova_params = {list(map(float,args.nenkova_params.split(',')))}

# ===============================================
# IMAGES AND SED
# ===============================================
NTHETA = 1
NPHI = 1

IMAGING = True
SED = True
SED_MONOCHROMATIC = False
FIX_SED_MONOCHROMATIC_WAVELENGTHS = False
SED_MONOCHROMATIC_min_lam = 0.1  # micron
SED_MONOCHROMATIC_max_lam = 1  # micron

filterdir = "{HOME}/agn/inputs/filters/"
filterfiles = ["{args.filter}.filter"]

npix_x = {args.bins}
npix_y = {args.bins}

IMAGING_TRANSMISSION_FILTER = False
filter_list = ["{HOME}/agn/inputs/filters/STIS_clear.filter"]
TRANSMISSION_FILTER_REDSHIFT = 0.001

# ===============================================
# GRID INFORMATION
# ===============================================
MANUAL_CENTERING = True
MANUAL_ORIENTATION = True
THETA = [{args.theta}]
PHI   = [{args.phi}]

# ===============================================
# OTHER INFORMATION
# ===============================================
solar = 0.013
PAH_frac = {{
    'usg': 0.0586,
    'vsg': 0.1351,
    'big': 0.8063
}}

# ===============================================
# DEBUGGING
# ===============================================
SOURCES_RANDOM_POSITIONS = False
SOURCES_IN_CENTER = False
STELLAR_SED_WRITE = True
SKIP_RT = False
SUPER_SIMPLE_SED = False
SKIP_GRID_READIN = False

CONSTANT_DUST_GRID = False 

N_MASS_BINS = 1

FORCE_STELLAR_AGES = False
FORCE_STELLAR_AGES_VALUE = 0.05

FORCE_STELLAR_METALLICITIES = False
FORCE_STELLAR_METALLICITIES_VALUE = 0.013
NEB_DEBUG = False

            """
    with open(f"{HOME}/agn/inputs/parameters_master_{args.halo}_{args.snapNum}_theta_{args.theta}_phi_{args.phi}.py", "w") as f:
        f.writelines(script)

def write_model_file(args):
    #info = pu.read_info(f"{args.hydro_dir}/{args.simul}/{args.halo}/info_{args.snapNum:03d}.txt", ['center'])
    #x,y,z = info['center'][0]

    pos = tu.trace_progenitors(treepath=f"{args.hydro_dir}/{args.simul}/{args.halo}/tree_0_0_0.dat")
    x,y,z = pos[args.snapNum-19]

    script = \
f"""
from __future__ import print_function
import os
#===============================================
# TUNEABLE PARAMETERS
#===============================================

hydro_dir = "{args.hydro_dir}"
PD_output_dir = "{HOME}/agn/outputs/{args.simul}/{args.halo}/{args.filter}/{args.bins}/"

#snapshot_num = 94
halo = "{args.halo}"
galaxy_num = "{args.halo}"
snapnum_str = "{args.snapNum:03d}"
galaxy_num_str = str(galaxy_num)
snapshot_name = '{args.simul}/{args.halo}/snap_{args.halo}_sf_x_2x_{args.snapNum:03d}.hdf5'

x_cent = {x*1e3}
y_cent = {y*1e3}
z_cent = {z*1e3}

Auto_TF_file = "snap{args.snapNum:03d}.logical"
Auto_dustdens_file = "snap{args.snapNum:03d}.dustdens"
TCMB = 2.73

#THETA = parser.get("Powderday-Run", "theta")
#THETA = list(map(int, THETA.split(",")))
#PHI = parser.get("Powderday-Run", "phi")
#PHI = list(map(int, PHI.split(",")))

#===============================================
#FILE I/O
#===============================================
inputfile  = "{HOME}/agn/outputs/{args.simul}/{args.halo}/{args.filter}/{args.bins}/output.{args.snapNum:03d}.theta_{args.theta}.phi_{args.phi}.rtin"
#inputfile  = "{HOME}/agn/outputs/{args.simul}/{args.halo}/{args.filter}/{args.bins}/darwin.{args.snapNum:03d}.theta_{args.theta}.phi_{args.phi}.rtin"
outputfile = "{HOME}/agn/outputs/{args.simul}/{args.halo}/{args.filter}/{args.bins}/output.{args.snapNum:03d}.theta_{args.theta}.phi_{args.phi}.rtout"
#outputfile = "{HOME}/agn/outputs/{args.simul}/{args.halo}/{args.filter}/{args.bins}/darwin.{args.snapNum:03d}.theta_{args.theta}.phi_{args.phi}.rtout"

"""

    with open(f"{HOME}/agn/inputs/parameters_model_{args.halo}_{args.snapNum}_theta_{args.theta}_phi_{args.phi}.py", "w") as f:
        f.writelines(script)


def run_powderday(args):
    output_path = f"{HOME}/agn/outputs/{args.simul}/{args.halo}/{args.filter}/{args.bins}" 
    if not os.path.isdir(output_path):
        os.system(f"mkdir -p {output_path}")

    os.system(f"{HOME}/miniconda3/envs/merger/bin/python {HOME}/powderday/pd_front_end.py {HOME}/agn/inputs/ parameters_master_{args.halo}_{args.snapNum}_theta_{args.theta}_phi_{args.phi} parameters_model_{args.halo}_{args.snapNum}_theta_{args.theta}_phi_{args.phi}")

    #if bool(args.remove_parameters):
    #    os.system(f"rm {HOME}/agn/inputs/parameters_master_{args.halo}_{args.snapNum}_theta_{args.theta}_phi_{args.phi}.py")
    #    os.system(f"rm {HOME}/agn/inputs/parameters_model_{args.halo}_{args.snapNum}_theta_{args.theta}_phi_{args.phi}.py")
    
def make_fits_image(args):
    IMAGE = True
    SED = True
    FITS_IMAGE = True
    FITS_SED = True

    redshift = mi.load_redshift(f"{args.hydro_dir}/{args.simul}", args.halo, args.snapNum)
    filter_file = f"{args.filter}.filter"
    filt,trans = mi.load_filter(f"{HOME}/agn/inputs/filters/{filter_file}")
    distance = Planck13.luminosity_distance(redshift).cgs

    if IMAGE:
        image_file = f"{HOME}/agn/outputs/{args.simul}/{args.halo}/{args.filter}/{args.bins}/output.{args.snapNum:03d}.theta_{args.theta}.phi_{args.phi}.rtout.image"
        try:
            image, hyperion_image = mi.make_image(image_file, distance, filt, trans)
            mi.save_image(image, hyperion_image, image_file, filter_file, distance, redshift, fits_output=FITS_IMAGE)
        except IOError:
            print("Failed: ", str(image_file))

    if SED:
        sed_file = f"{HOME}/agn/outputs/{args.simul}/{args.halo}/{args.filter}/{args.bins}/output.{args.snapNum:03d}.theta_{args.theta}.phi_{args.phi}.rtout.sed"
        try:
            sed = mi.make_sed(sed_file, distance)
            mi.save_sed(sed, sed_file, filter_file, distance, redshift, fits_output=FITS_SED)
        except IOError:
            print("Failed: ", str(sed_file))

def read_filter_wheel(filter_name):
    import configparser
    parser = configparser.ConfigParser()
    parser.read_file(open(f"{HOME}/agn/inputs/filters/filter_wheel.txt"))
    try:
        pivot_wavelength, FWHM, SB_lim = list(map(float,parser['FILTER INFO'][filter_name].split(',')))
    except:
        pivot_wavelength, FWHM = list(map(float, parser['FILTER INFO'][filter_name].split(',')))
        SB_lim = 25.
    
    return {filter_name : [pivot_wavelength, FWHM, SB_lim]}


def add_psf_noise(args):
    ff    = f"{HOME}/agn/outputs/{args.simul}/{args.halo}/{args.filter}/{args.bins}/output.{args.snapNum:03d}.theta_{args.theta}.phi_{args.phi}.image.fits"
    ffout = f"{HOME}/agn/outputs/{args.simul}/{args.halo}/{args.filter}/{args.bins}/output.{args.snapNum:03d}.theta_{args.theta}.phi_{args.phi}.image.mock.fits"

    ptm.output_pristine_fits_image(ff, ffout, filt_wheel)
    ptm.convolve_with_fwhm(ffout, filt_wheel)
    ptm.add_simple_noise(ffout, sb_maglim=float(filt_wheel[args.filter]["sb_lim"]), ext_name="MockImage_sblim")


def insert_field(args): #field, path=f"{HOME}/agn/inputs/fields/"):
    ffout = f"{HOME}/agn/outputs/{args.simul}/{args.halo}/{args.filter}/{args.bins}/output.{args.snapNum:03d}.theta_{args.theta}.phi_{args.phi}.image.mock.fits"

    field_info = {'COS': ['hlsp_candels_hst_wfc3_cos-tot-multiband_f160w_v1_cat.fits', 'hlsp_candels_hst_wfc3_cos-tot_f160w_v1.0_drz.fits']}
    cat = fits.open(f"{HOME}/agn/inputs/fields/{field_info[field][0]}")
    field = fits.open(f"{HOME}/agn/inputs/fields/{field_info[field][1]}")
    
    candels_args = rs.make_candels_args(field_info, f'{HOME}/agn/inputs/fields/', field_name='COS', cutout_size=512)
    new_im, real_im = rs.ObsRealism(ffout, candels_args)



def calc_morphology(args):
    ffout = f"{HOME}/agn/outputs/{args.simul}/{args.halo}/{args.filter}/{args.bins}/output.{args.snapNum:03d}.theta_{args.theta}.phi_{args.phi}.image.mock.fits"
    fo = fits.open(ffout, "append")
    ext_name = "MockImage_sblim"  ## CHANGE IT TO REAL SIM AFTER INJECTING FIELDS!!!
    hdu = fo[ext_name]

    # detect sources
    typical_kpc_per_arcsec = converter(1, 1/snap_at[args.snapNum]-1)

    kernel_kpc_fwhm = 35. * float(filt_wheel[hdu.header["FILTER"]]["FWHM"]) #5.0
    kernel_arcsec_fwhm = kernel_kpc_fwhm / typical_kpc_per_arcsec
    kernel_pixel_fwhm = kernel_arcsec_fwhm / hdu.header["PIXSIZE"]

    sigma = kernel_pixel_fwhm * gaussian_fwhm_to_sigma
    nsize = int(5 * kernel_pixel_fwhm)
    kernel = Gaussian2DKernel(sigma, x_size=nsize, y_size=nsize)

    bkg_estimator = photutils.background.MedianBackground()
    bkg = photutils.background.Background2D(hdu.data, (40,40), bkg_estimator=bkg_estimator)
    thresh = bkg.background + (5.0 * bkg.background_rms)

    segmap_obj = photutils.segmentation.detect_sources(
        scipy.ndimage.convolve(hdu.data, kernel), thresh, npixels=10
    )

    # deblend sources
    gain = float(filt_wheel[hdu.header['FILTER']]['sb_lim'])
    errmap = calc_total_error(hdu.data, bkg.background_rms, effective_gain=gain)
    segmap_obj = photutils.segmentation.deblend_sources(
        scipy.ndimage.convolve(hdu.data, kernel), segmap_obj, npixels=10
    )
    segmap = segmap_obj.data
    source = photutils.segmentation.SourceCatalog(hdu.data, segmap_obj, error=errmap)
    props_table = source.to_table()

    fluxes = np.asarray(props_table["segment_flux"])

    brightest_index = np.argmax(fluxes) # this source is the brightest
    brightest_id = props_table['label'][brightest_index]
    npix = fo["MockImage_sblim"].data.shape[0]
    center_slice = segmap_obj.data[int(npix/2)-2:int(npix/2)+2,int(npix/2)-2:int(npix/2)+2]
    center_source_id = props_table['label'][props_table['label']==center_slice[0,0]].data[0]
    central_index = props_table['label']==center_slice[0,0]

    try:
        source_morph = statmorph.SourceMorphology(
            hdu.data-bkg.background, segmap_obj.data, center_source_id, weightmap=errmap,# **morph_params
        )
        nhdu = fits.ImageHDU()
        nhdu.header["EXTNAME"] = "SOURCE_MORPH"

        if source_morph is not None:
            for key,value in morph_params.items():
                nhdu.header[key] = source_morph[value]
        fo.append(nhdu)
        fo.flush()
        fo.close()

    except (KeyError, IndexError, AttributeError, ValueError, TypeError) as err:
        print(err, "-", ffout, "not processed, skipping fit.")

def remove(args):
    os.system(f"{HOME}/agn/inputs/parameters_master_{args.halo}_{args.snapNum}_theta_{args.theta}_phi_{args.phi}.py")
    os.system(f"{HOME}/agn/inputs/parameters_model_{args.halo}_{args.snapNum}_theta_{args.theta}_phi_{args.phi}.py")
    os.system(f"{HOME}/agn/outputs/{args.simul}/{args.halo}/{args.filter}/{args.bins}/SKIRT*")
    os.system(f"{HOME}/agn/outputs/{args.simul}/{args.halo}/{args.filter}/{args.bins}/grid_physical_properties*")
    os.system(f"{HOME}/agn/outputs/{args.simul}/{args.halo}/{args.filter}/{args.bins}/cell_info*")
    os.system(f"{HOME}/agn/outputs/{args.simul}/{args.halo}/{args.filter}/{args.bins}/*rtin*")
    os.system(f"{HOME}/agn/outputs/{args.simul}/{args.halo}/{args.filter}/{args.bins}/bh_sed*")
    os.system(f"{HOME}/agn/outputs/{args.simul}/{args.halo}/{args.filter}/{args.bins}/*image.fits")



def main(args):
    
    write_master_file(args)
    write_model_file(args)

    # run powderday
    run_powderday(args)
    
    # make fits file
    make_fits_image(args)
    
    # add PSF + noise
    add_psf_noise(args)
    
    # field data
    #insert_field(args)
    
    # calculate morphologies
    calc_morphology(args)

    remove(args)


if __name__=="__main__":
    p = argparse.ArgumentParser(description="from powderday to morphology calculation")
    
    # file system
    p.add_argument("--remove_parameters", dest="remove_parameters", type=int, default=0, help="0 if you don't want to remove parameter files")
    
    # powderday command
    p.add_argument("--simul", dest="simul", default="Swallow")
    p.add_argument("--halo", dest="halo")
    p.add_argument("--snapNum", type=int, dest="snapNum")
    p.add_argument("--boxsize", dest="boxsize", type=float, default=None, help='Unit is in kpc')
    p.add_argument("--bins", dest='bins', type=int, default=None)
    p.add_argument("--filter", dest="filter")
    
    p.add_argument("--theta", dest="theta", default=78)
    p.add_argument("--phi", dest="phi", default=1)

    p.add_argument("--dust", dest="dust", default="d03_3.1_6.0_A.hdf5")
    p.add_argument("--metallicity", dest="metallicity", default="fsps/ISOCHRONES/Padova/Padova2007/zlegend.dat")
    p.add_argument("--BH_model", dest="BH_model", default="Nenkova")
    p.add_argument("--BH_file", dest="BH_file", default="agn/inputs/BH_model/clumpy_models_201410_tvavg.hdf5")
    p.add_argument("--nenkova_params", dest="nenkova_params", default="5,30,0,1.5,30,40")

    # path
    p.add_argument("--hydro_dir", dest="hydro_dir", default="/hdfs/user/phynah/Fiducial_MrAGN/")
    p.add_argument("--filter_dir", dest="filter_dir", default="agn/inputs/filters/")
    p.add_argument("--dust_dir", dest="dust_dir", default="")

    args = p.parse_args()

    redshift = mi.load_redshift(f"{args.hydro_dir}/{args.simul}", args.halo, args.snapNum)

    if args.boxsize==None:
        if args.bins==None:
            raise ValueError("You should set one of boxsize or bins.")
        boxsize_in_arcsec = float(filt_wheel[args.filter]["pixel_size_arcsec"]) * args.bins
        args.boxsize = converter(boxsize_in_arcsec, redshift)
    
    if args.bins==None:
        if args.boxsize==None:
            raise ValueError("You should set one of boxsize or bins.")
        pixel_in_kpc = converter(float(filt_wheel[args.filter]['pixel_size_arcsec']), redshift)
        args.bins = round(args.boxsize/pixel_in_kpc)
        
    print(args.snapNum, redshift, boxsize_in_arcsec, args.boxsize)
    #args.bins = 512
    #args.boxsize = 134.3417344
    main(args)
