import os
import numpy as np
import pandas as pd
from astropy.io import fits

halo_list = [h for h in os.listdir("/home2/hannah.jhee/agn/outputs/Fiducial_MrAGN")]

for hid in halo_list:
print(f"HALO : {hid}")
    for snapNum in range(19,61):
        filename = glob.glob(f"/home2/hannah.jhee/agn/outputs/Fiducial_MrAGN/{hid}/NIRCam_F277W/512/output.{snapNum:03d}.*.image.mock.fits")
        if len(filename)<3:
            print(f"   Snapshot {snapNum:03d} NOT CREATED")
        else:
            hdul = fits.open(filename)
            header = hdul["SOURCE_MORPH"].header
            G,M20,ASYM,CONC = list(map(lambda x: header[x], ["GINI","M20","ASYM","CONC"]))
            hdul.close()
            print(G,M20,ASYM,CONC)
