import argparse
from astropy.io import fits
import matplotlib.pyplot as plt
import numpy as np

def make_img(R, G, B, N, output):
    bins = int(R.split('/')[6].strip())
    image = np.zeros((bins,bins,3), dtype=float)
    image[:,:,0] = fits.open(R)["MockImage_sblim"].data
    image[:,:,1] = fits.open(G)["MockImage_sblim"].data
    image[:,:,2] = fits.open(B)["MockImage_sblim"].data

    image_s = np.log10(image+0.1)

    max_val = image_s.max()
    image_s /= max_val

    bmin = int(bins/2 - N/2)
    bmax = int(bins/2 + N/2)
    plt.imshow(image_s[bmin:bmax,bmin:bmax,:])
    plt.gca().axis("off")
    plt.savefig(output)
    

def main(args):
    R,G,B = args.R, args.G, args.B
    N = args.cutout
    output = args.output

    make_img(R, G, B, N, output)


if __name__=="__main__":
    p = argparse.ArgumentParser()
    p.add_argument("-R", dest="R", default="/home2/hannah.jhee/agn/outputs/m0209/NIRCam_F277W/512/output.theta_45.phi_0.034.image.mock.fits")
    p.add_argument("-G", dest="G", default="/home2/hannah.jhee/agn/outputs/m0209/NIRCam_F200W/512/output.theta_45.phi_0.034.image.mock.fits")
    p.add_argument("-B", dest="B", default="/home2/hannah.jhee/agn/outputs/m0209/NIRCam_F150W/512/output.theta_45.phi_0.034.image.mock.fits")
    p.add_argument("--cutout", dest="cutout", type=int, default=64)
    p.add_argument("-o", "--output", dest="output", default="./rgb.pdf")
    args = p.parse_args()

    main(args)
