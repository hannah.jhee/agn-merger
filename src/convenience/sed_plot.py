import matplotlib
matplotlib.use('Agg')

import argparse
import numpy as np
import matplotlib.pyplot as plt
from hyperion.model import ModelOutput
from astropy.cosmology import Planck13
from astropy import units as u
from astropy import constants

#========================================================
#MODIFIABLE HEADER (make this a function later with argv)
p = argparse.ArgumentParser()
p.add_argument("-z", dest="z", default=1.5, type=float)
p.add_argument("--sed_file", dest="sed_file", default="/home/gpuadmin/agn/agn/outputs/m0215/WFC_F160W/512/output.034.rtout.sed")
args = p.parse_args()
#========================================================

fig = plt.figure()
ax = fig.add_subplot(1,1,1)

run = args.sed_file
m = ModelOutput(run)
wav,flux = m.get_sed(inclination='all',aperture=-1)

wav  = np.asarray(wav)*u.micron #wav is in micron
wav *= (1.+args.z)

flux = np.asarray(flux)*u.erg/u.s
dl = Planck13.luminosity_distance(args.z)
dl = dl.to(u.cm)
    
flux /= (4.*3.14*dl**2.)
    
nu = constants.c.cgs/(wav.to(u.cm))
nu = nu.to(u.Hz)

flux /= nu
flux = flux.to(u.mJy)


for i in range(flux.shape[0]):
    ax.loglog(wav,flux[i,:], c='k')

ax.set_xlabel(r'$\lambda$ [$\mu$m]')
ax.set_ylabel('Flux (mJy)')
#ax.set_ylim([1,1e8])
#ax.set_xlim(0.05,15000)
ax.grid(alpha=0.3, linestyle='--')

fig.savefig('./sed.png')
