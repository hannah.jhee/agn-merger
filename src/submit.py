import os
import numpy as np

#halos = 'm0175 m0204 m0209 m0215 m0305 m0501 m0664 m0858 m0908 m0948'.split()
simul = 'Fiducial_MrAGN'
halo = 'm0209' #os.listdir(f"/home2/hannah.jhee/halos/{simul}")
Filter = 'NIRCam_F277W'
snapNum = 60

script = \
f"""#!/bin/bash
#SBATCH --nodes=1  # 1
#SBATCH --ntasks-per-node=8    # Cores per node 8
#SBATCH --partition=cpu1        # Partition Name (cpu1, gpu1, hgx)
##
#SBATCH --job-name={halo}
#SBATCH -o {halo}.out         # STDOUT
#SBATCH -e {halo}.err         # STDERR
##

hostname
date

## 


source /home2/hannah.jhee/.bashrc
conda activate merger

module load GNUcompiler/9.3.0

"""

theta = 0#np.random.randint(0,181)
phi   = 0#np.random.randint(0,361)
script += f"python run.py --remove_parameters 0 --simul {simul} --halo {halo} --snapNum {snapNum} --bins 512 --filter {Filter} --theta {theta} --phi {phi} --hydro_dir /home2/hannah.jhee/halos/\n"

with open(f"/home2/hannah.jhee/agn/src/submit_{halo}.sh","w") as f:
    f.writelines(script)

os.system(f"sbatch /home2/hannah.jhee/agn/src/submit_{halo}.sh")
os.system(f"rm /home2/hannah.jhee/agn/src/submit_{halo}.sh")
