import scipy
import numpy as np
import astropy.io.fits as fits
def ObsRealism(
    inputName,
    candels_args={
        "candels_field": "COS",  # candels field
        #"candels_ra": 236.1900,  # ra for image centroid
        #"candels_dec": -0.9200,  # dec for image centroid
    },
):

    """
    Add realism to idealized unscaled image.
    """

    # img header and data
    with fits.open(inputName, mode="readonly") as hdul:
        # img header
        header = hdul["MockImage_sblim"].header
        # img data
        img_data = hdul["MockImage_sblim"].data

    # add image to real sdss sky
    """
    Extract field from galaxy survey database using
    effectively weighted by the number of galaxies in
    each field. For this to work, the desired field
    mask should already have been generated and the
    insertion location selected.
    """
    colc = candels_args["candels_colc"]
    rowc = candels_args["candels_rowc"]
    real_im = candels_args["candels_im"]

    # convert to integers
    colc, rowc = int(np.around(colc)), int(np.around(rowc))

    # add real sky pixel by pixel to image in nanomaggies
    corr_ny, corr_nx = real_im.shape
    ny, nx = img_data.shape

    for xx in range(nx):
        for yy in range(ny):
            corr_x = int(colc - nx / 2 + xx)
            corr_y = int(rowc - ny / 2 + yy)
            if (
                corr_x >= 0
                and corr_x <= corr_nx - 1
                and corr_y >= 0
                and corr_y <= corr_ny - 1
            ):
                img_data[yy, xx] += real_im[corr_y, corr_x]

    return img_data, real_im


"""
Script executions start here. This version grabs a corrected image
based on a basis set of galaxies from a database, runs source 
extractor to produce a mask, and selects the location in which to
place the image in the SDSS sky. The final science cutout includes
PSF blurring (real SDSS), SDSS sky from the corrected image, and 
Poisson noise added. The final image is in nanomaggies.
"""

# get field,column,row from database data
def rrcf_radec(field_info, input_dir, field_name='COS', cutout_size=244):
    from astropy.wcs import WCS
    from astropy.nddata import Cutout2D

    if field_name is None:
        field_name = np.random.choice(list(field_info.keys()))

    catalog = fits.getdata(input_dir + field_info[field_name][0], 1)
    field, field_header = fits.getdata(
        input_dir + field_info[field_name][1], header=True
    )

    # randomly select from basis set
    print(field_name)
    #catalog = catalog[(catalog['FLAGS'] == 0) & (catalog['DEEP_SPEC_Z'] < 3) & (catalog['DEEP_SPEC_Z'] > 0.5)]
    index = np.random.randint(low=0, high=len(catalog) - 1)

    position = catalog["X_image"][index], catalog["Y_image"][index]
    size = (cutout_size, cutout_size)
    im = Cutout2D(field, position, size).data

    # convert to counts nanojanskies, where PHOTFNU is inverse sensitivity in units Jy*sec/electron
    im *= field_header["PHOTFNU"] * 1e9

    segmap = detect_sources(im)

    ## Run photutils

    # get info from mask header
    mask_nx, mask_ny = segmap.shape
    # define an initial pixel location by row,col
    colc = np.random.randint(low=int(0.1 * mask_nx), high=int(0.9 * mask_nx))
    rowc = np.random.randint(low=int(0.1 * mask_ny), high=int(0.9 * mask_ny))
    # iterate until the pixel location does not overlap with existing source
    while segmap[rowc, colc] != 0:
        colc = np.random.randint(low=int(0.1 * mask_nx), high=int(0.9 * mask_nx))
        rowc = np.random.randint(low=int(0.1 * mask_ny), high=int(0.9 * mask_ny))
    # get wcs mapping
    w = WCS(field_header)
    # determine ra,dec to prevent image registration offsets in each band
    ra, dec = w.wcs_pix2world(colc, rowc, 1, ra_dec_order=True)
    return field_name, ra, dec, colc, rowc, im


def make_candels_args(field_info, INPUT_DIR, field_name, cutout_size):
    field_name, ra, dec, colc, rowc, im = rrcf_radec(field_info, INPUT_DIR, field_name, cutout_size)
    candels_args = {
        "candels_field": field_name,  # candels field
        "candels_ra": ra,  # ra for image centroid
        "candels_dec": dec,  # dec for image centroid
        "candels_colc": colc,  # pixel x-coordinate for image centroid
        "candels_rowc": rowc,  # pixel y-coordinate for image centroid
        "candels_im": im,  # candels cutout
    }
    return candels_args


def detect_sources(image, PIXSIZE=0.13):
    import photutils
    from astropy.stats import gaussian_fwhm_to_sigma
    from astropy.convolution import Gaussian2DKernel

    # Run basic source detection

    # build kernel for pre-filtering.  How big?
    # don't assume redshift knowledge here
    typical_kpc_per_arcsec = 8.0

    kernel_kpc_fwhm = 5.0
    kernel_arcsec_fwhm = kernel_kpc_fwhm / typical_kpc_per_arcsec
    kernel_pixel_fwhm = kernel_arcsec_fwhm / PIXSIZE

    sigma = kernel_pixel_fwhm * gaussian_fwhm_to_sigma
    nsize = int(5 * kernel_pixel_fwhm) - 1
    kernel = Gaussian2DKernel(sigma, x_size=nsize, y_size=nsize)

    bkg_estimator = photutils.background.MedianBackground()
    bkg = photutils.background.Background2D(image, (50, 50), bkg_estimator=bkg_estimator)
    thresh = bkg.background + (5.0 * bkg.background_rms)
    segmap_obj = photutils.segmentation.detect_sources(
        scipy.ndimage.convolve(image, kernel), thresh, npixels=10
    )

    segmap_obj = photutils.segmentation.deblend_sources(
        scipy.ndimage.convolve(image, kernel), segmap_obj, npixels=10, nlevels=32, contrast=0.01
    )

    segmap = segmap_obj.data

    return segmap
