# UDS field
wget https://archive.stsci.edu/hlsps/candels/uds/catalogs/v1/hlsp_candels_hst_wfc3_uds-tot-multiband_f160w_v1_cat.fits
# EGS field
wget https://archive.stsci.edu/hlsps/candels/egs/catalogs/v1/hlsp_candels_hst_wfc3_egs-tot-multiband_f160w_v1_cat.fits
# GS field
wget https://archive.stsci.edu/hlsps/candels/goods-s/catalogs/v1/hlsp_candels_hst_wfc3_goodss-tot-multiband_f160w_v1_cat.fits
# GN field
wget https://archive.stsci.edu/hlsps/candels/goods-n/catalogs/v1/hlsp_candels_hst_wfc3_goodsn-barro19_multi_v1-1_photometry-cat.fits
# COS field
wget https://archive.stsci.edu/hlsps/candels/cosmos/catalogs/v1/hlsp_candels_hst_wfc3_cos-tot-multiband_f160w_v1_cat.fits
wget https://irsa.ipac.caltech.edu/data/COSMOS/images/candels/hlsp_candels_hst_wfc3_cos-tot_f160w_v1.0_drz.fits
