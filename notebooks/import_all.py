import os
from tqdm import tqdm
import h5py
import glob
import pickle
import numpy as np
from scipy import interpolate
from operator import itemgetter
from astropy.io import fits
from astropy import units as u
from astropy import constants as c
import pandas as pd
from astropy.cosmology import Planck13

import scipy
import photutils
from photutils.utils import calc_total_error
from photutils.segmentation import SegmentationImage
from astropy.stats import gaussian_fwhm_to_sigma
from astropy.convolution import Gaussian2DKernel
import statmorph

import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
#mpl.rc('text', usetex=True)
#mpl.rc('font', size=14)
#mpl.rc('legend', fontsize=13)
#mpl.rc('text.latex', preamble=r'\usepackage{cmbright}')

from matplotlib import font_manager
font_manager.fontManager.addfont("/home/gpuadmin/AVHersheySimplexMedium.ttf")

# set font
plt.rcParams['font.family'] = 'AVHershey Simplex'
plt.rcParams['font.size'] = 12
plt.rcParams['mathtext.default'] = 'regular'
plt.rcParams['mathtext.rm'] = 'AVHershey Simplex'
plt.rcParams['mathtext.it'] = 'AVHershey Simplex'

import pandas as pd
with open(f"/data2/agn_trigger/snap_to_at.pickle","rb") as handle:
    snap_at = pickle.load(handle)

class Tree:
    def __init__(self, simul, hid):
        self.simul = simul
        self.hid = hid
        self.tree = None
        self.mmps = None
        self.merger = None

    def load_tree(self, file_format="/home/gpuadmin/HaloFinders/run_rockstar/results/%s/Rockstar/trees/tree_0_0_0.dat"):
        self.tree = tree_data(file_format%self.hid)
        self.tree['redshift'] = 1/np.array(itemgetter(*self.tree.Snap_idx.values)(snap_at))-1
        self.tree = self.tree[self.tree.Snap_idx>=4]
        print("Calculating stellar masses within 0.1 rvir")
        SMs = []
        for snapNum in tqdm(range(4,95), desc=self.hid):
            if snapNum in range(19,61):
                tree = self.tree[self.tree.Snap_idx==snapNum]
                with h5py.File(f"/data2/{self.simul}/{self.hid}/snap_{self.hid}_sf_x_2x_{snapNum:03d}.hdf5") as f:
                    scoords = f["PartType4"]["Coordinates"][:,:] * 1e-3   # in Mpc
                    smasses = f["PartType4"]["Masses"][:]
                SM = list(map(lambda dfiter: smasses[np.linalg.norm(scoords-dfiter[1][['x','y','z']].values, axis=1) < 0.1 * dfiter[1]['Rvir']*1e-3].sum()*1e10,
                            tree[['x','y','z','Rvir']].iterrows()))
                SMs += SM
                del scoords,smasses,SM
            else:
                tree = self.tree[self.tree.Snap_idx==snapNum]
                SMs += list(np.zeros(len(tree), dtype=float))
        self.tree['SM'] = SMs

        print("Getting most massive progenitors")
        self._get_mmps()
        self.tree = self.tree[(self.tree.Snap_idx>=19) & (self.tree.Snap_idx<=60)]
        
    def _get_mmps(self):
        snapNum = 94
        mmp_id = -1

        self.mmps = pd.DataFrame(columns=self.tree.columns)
        self.merger = pd.DataFrame(columns=self.tree.columns)
        while True:
            tr = self.tree[self.tree.Snap_idx == snapNum]
            tr = tr[tr.desc_id==mmp_id] # 이거 추가해야함...ㅜㅜ
            mmp = tr[tr.Mvir.values==tr.Mvir.values.max()]
            mmp_id = mmp.id.iat[0]
            num_progs = mmp.num_prog.iat[0]

            self.mmps = pd.concat([self.mmps,mmp])
            if (snapNum==19) | (num_progs==0): break

            if num_progs>1:
                self.merger = pd.concat([self.merger, tr])
        
            snapNum -= 1

#        self.mmps = self.mmps.sort_values(by="Snap_idx", ascending=True)


        self.mmps = self.mmps.sort_values(by="Snap_idx", ascending=True)
        self.merger = self.merger.sort_values(by="Snap_idx", ascending=True)
        self.mmps = self.mmps[self.mmps.Snap_idx<61]
        self.merger = self.merger[self.merger.Snap_idx<61]


import os
import h5py
import glob
import pickle
import numpy as np
import pandas as pd

from operator import itemgetter
from tqdm import tqdm
from astropy.io import fits
from astropy import constants as c
from astropy import units as u
from astropy.cosmology import Planck13
from hyperion.model import ModelOutput

def redshift_to_wavelength(redshift):
    # Calculate the observed wavelength of the Lyman break using the formula:
    # observed wavelength = rest wavelength * (1 + redshift)
    rest_wavelength = 91.2 # nm
    observed_wavelength = rest_wavelength * (1 + redshift)
    return observed_wavelength


def read_info(filename, fields):
    lines = open(filename).readlines()
    lines = [line.split(':') for line in lines]

    info_fields = [line[0].strip() for line in lines]
    
    info = dict()
    for field in fields:
        try:
            index = info_fields.index(field)
        except:
            raise ValueError(f"{field} does not exist in this info file. Available fields are: {info_fields[2:-1]}")

        x = lines[index][1].split('[')
        for i in range(len(x)):
            x[i] = x[i].strip()
            x[i] = x[i].strip(']')
        
        if len(x)!=1:
            if len(x[0])==0:
                del x[0]
            if len(x[-1])==0:
                del x[-1]

            val,unit = x
            val = val.split()
            for i in range(len(val)):
                try:
                    val[i] = float(val[i])
                except:
                    val[i] = None

        else: # if len(x)==1
            try:
                val = float(x[0])
            except:
                val = None
            unit = None
        info[field] = [val, unit]

    return info

def calculate_bolometric_luminosity(wavelengths, fluxes):
    # Convert wavelength from nm to cm
    wavelengths_cm = wavelengths * 1e-7
    
    # Convert flux from mJy to erg/s/cm^2/Hz
    fluxes_ergs = fluxes * 1e-26
    
    # Calculate the frequency corresponding to each wavelength
    c = 2.99792458e10 # Speed of light in cm/s
    frequencies = c / wavelengths_cm
    
    # Calculate the integral of flux*frequency over all frequencies
    integrand = fluxes_ergs * frequencies
    integral = np.trapz(integrand, x=frequencies)
    
    # Calculate the bolometric luminosity in erg/s
    luminosity = 4 * np.pi * integral
    
    return luminosity


### 
def tree_data(tree_path):
    data = np.loadtxt(tree_path, skiprows=49)
    columns = open(tree_path).readline().split()
    columns[0] = columns[0][1:]
    columns[0] = columns[0][1:]
    for i in range(len(columns)):
        col = columns[i]
        if col.endswith(')'):
            columns[i] = col.split('(')[0]        
    df = pd.DataFrame(columns=columns, data=data)
    offset = 94 - df['Snap_idx'].max()
    df['Snap_idx'] += offset
    
    return df.sort_values(by="Snap_idx", ascending=True).reset_index().drop("index",axis=1)


def trace_progenitors(treepath,massis=False):
    tree = tree_data(treepath)
    pos = []
    mass = []

    snapNum = 94
    mmp_id = -1
    while True:
        tr = tree[tree.Snap_idx == snapNum]
        tr = tr[tr.desc_id==mmp_id]
        mmp = tr[tr.Mvir.values==tr.Mvir.values.max()]
        mmp_id = mmp.id.iat[0]
        num_progs = mmp.num_prog.iat[0]


        if snapNum<=60:
            pos.append(mmp[['x','y','z']].values.tolist()[0])
            if massis:
                mass.append(mmp.Mvir.iat[0])
        if snapNum==19:
            break

        snapNum -= 1
    if massis:
        return mass[::-1]
    else:
        return pos[::-1]

    
def take_merger(tree, threshold=0.25):
    snapNum = 94
    tr = tree[tree.Snap_idx==snapNum]
    mmp = tr[tr.SM.values==tr.SM.values.max()]
    mmp_id = mmp.id.iat[0]
    num_progs = mmp.num_prog.iat[0]
    i = 1
    
    snaps = []
    ratio = []
    while True:
        snapNum -= i
        tr = tree[tree.Snap_idx==snapNum]
        progs = tr[tr.desc_id==mmp_id]
        if len(progs)!=num_progs:
            raise ValueError("Number of progenitors is not matched with data!")

        if num_progs>1: # if num_progs==1, then this galaxy hasn't gone through merger
            Mvir = progs['SM'].values / progs['SM'].values.max()
            idx = Mvir>threshold

            if idx.sum()>1:
                #print(f"Major Merger happened after this snapshot({snapNum}).")
                #print(f"    {Mvir[Mvir<1].max()}")
                snaps.append(snapNum)
                ratio.append(Mvir[Mvir<1].max())

            mmp = progs[progs.SM.values==progs.SM.values.max()]
        elif num_progs==1:
            mmp = progs
        mmp_id = mmp.id.iat[0]
        num_progs = mmp.num_prog.iat[0]

        if num_progs==0:
            break
    return snaps, ratio


with open("/data2/agn_trigger/snap_to_at.pickle","rb") as handle:
    snap_at = pickle.load(handle)
    
whole_redshift = 1/np.array(itemgetter(*range(0,95))(snap_at)) -1